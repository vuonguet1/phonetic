import pymongo

#to connect with the localhost
connection = pymongo.MongoClient('localhost', 27017)
#create your database
database = connection['letrungdev-db']
print("Collection Created..")
#create your collection
collection = database['phonetic']
print("Database Connected..")


#PHONETIC
class phonetic:

    #INSERT
    def insert_data(data):
        document = collection.insert_one(data)
        return document.inserted_id

    #FIND
    def get_single_data(document_id):
        data = collection.find_one({'_id': ObjectId(document_id)})
        return data

    def get_multiple_data():
        data = collection.find()
        return list(data)

    #UPDATE
    def update_or_create(document_id, data):
        document = collection.update_one({'_id': ObjectId(document_id)}, {"$set": data}, upsert=True)
        return document.acknowledged

    def update_existing(document_id, data):
        document = collection.update_one({'_id': ObjectId(document_id)}, {"$set": data})
        return document.acknowledged

    #REMOVE
    def remove_data(document_id):
        document = collection.delete_one({'_id': ObjectId(document_id)})
        return document.acknowledged

#CLOSE DATABASE
connection.close()